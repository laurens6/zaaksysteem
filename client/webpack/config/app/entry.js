const apps = require('../../library/apps');

module.exports = apps.reduce((result, app) => {
  result[app] = `./src/${app}/index.js`;

  return result;
}, {});
