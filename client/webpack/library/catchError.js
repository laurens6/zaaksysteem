const { red } = require('chalk');

function catchError(error, message) {
  console.error(`${red('Fatal error:')} ${error.message}`);

  if (message) {
    console.info(message);
  }

  process.exit(1);
};

module.exports = catchError;
