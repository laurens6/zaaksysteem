import angular from 'angular';
import template from './index.html';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import groupListViewModule from './groupListView';

export default {

	moduleName:
		angular.module('Zaaksysteem.meeting.groupList', [
			resourceModule,
			groupListViewModule,
			composedReducerModule
		])
		.controller('groupListController', [ '$scope', 'appConfig', ( $scope, appConfig ) => {

			$scope.appConfig = appConfig.data;

		}])
		.name,
	config: [
		{
			route: {
				url: '',
				scope: {
					appConfig: '&'
				},
				template,
				controller: 'groupListController'
			},
			state: 'groupList'
		}
	]
};
