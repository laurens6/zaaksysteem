export default class KeyboardNavigableListController {

	static get $inject() {
		return ['$scope', '$element', '$document'];
	}

	constructor( $scope, $element, $document ) {
		const ctrl = this;
		let highlighted;
		let input;

		let highlight = ( item, event ) => {
			highlighted = item;
			ctrl.onKeyHighlight({
				$item: item,
				$event: event
			});
		};

		let commit = ( event ) => {
			if (highlighted) {
				ctrl.onKeyCommit({
					$item: highlighted,
					$event: event
				});
			}

			event.preventDefault();
			event.stopPropagation();
		};

		let moveUp = ( event ) => {
			let items = ctrl.highlightableItems();

			if (!items || !items.length) {
				return;
			}

			let index = items.indexOf(highlighted);

			if (index === 0 || index === -1) {
				index = items.length - 1;
			} else {
				index -= 1;
			}

			highlight(items[index], event);
		};

		let moveDown = ( event ) => {
			let items = ctrl.highlightableItems();

			if (!items || !items.length) {
				return;
			}

			let index = items.indexOf(highlighted);

			if (index === items.length - 1 || index === -1) {
				index = 0;
			} else {
				index += 1;
			}

			highlight(items[index], event);
		};

		let onKeyDown = ( event ) => {
			switch (event.keyCode) {
			case 13:
				$scope.$apply(() => {
					commit(event);
				});
				break;
			case 9:
				if (ctrl.commitOnTab()) {
					$scope.$apply(() => {
						commit(event);
					});
				}
				break;
			case 38:
				event.preventDefault();
				$scope.$apply(() => {
					moveUp(event);
				});
				break;
			case 40:
				event.preventDefault();
				$scope.$apply(() => {
					moveDown(event);
				});
				break;
			}
		};

		let enableKeyboardNavigation = function () {
			input.on('keydown', onKeyDown);
		};

		let disableKeyboardNavigation = function () {
			input.off('keydown', onKeyDown);
		};

		if (ctrl.keyInputDelegate()) {
			input = ctrl.keyInputDelegate().input;
		} else {
			input = $element;
		}

		let onFocus = () => {
			$scope.$evalAsync(() => {
				enableKeyboardNavigation();
			});
		};

		let onBlur = () => {
			$scope.$evalAsync(() => {
				disableKeyboardNavigation();
			});
		};

		input.on('focus', onFocus);

		input.on('blur', onBlur);

		$scope.$watch(ctrl.highlightableItems, () => {
			highlight(undefined);
		});

		if ($document[0].activeElement === input[0]) {
			enableKeyboardNavigation();
		}

		$scope.$on('$destroy', () => {
			input.off('focus', onFocus);
			input.off('blur', onBlur);
		});
	}

}
