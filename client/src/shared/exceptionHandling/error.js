import get from 'lodash/get';
import ZaaksysteemException from './ZaaksysteemException';

export {
  apiV1Error,
  translateErrorToUI,
};

const errorMapping = {
  'api/case/find_case' :
    'De zaak kon niet worden gevonden. Mogelijk is de zaak verwijderd',

  'api/case/authorization' :
    'U heeft niet voldoende rechten om deze zaak te bekijken.',

  'case/reject/incomplete_distributors':
    'Er zijn geen zaakdistributeurs geconfigureerd, de zaak kan niet geweigerd worden',
};

function translateErrorToUI(response, msg) {

  const version = get(response.data, 'api_version');

  let error = {};
  // TODO: Make a distinction based on type and not on version
  if (version === undefined) {
    error = response;
  }
  else if (version === 1) {
    const resultType = get(response.data.result, 'type');
    if (resultType !== 'exception') {
      throw new ZaaksysteemException(
        'api/v1/error/type/mismatch', 'This is not a ZS api/v1 exception'
      );
    }
    error = apiV1Error(response.data);
  }

  let rv = get(errorMapping, error.type);
  if (rv !== undefined) {
    return rv;
  }

  if (msg === undefined || !msg.trim().length) {
    if (version === undefined) {
      return `Er is een fout "${error.type}" opgetreden, neem contact op met uw beheerder`;
    }
    else if (version === 1) {
      return `Er is een fout opgetreden (${error.requestID}), neem contact op met uw beheerder`;
    }
    else {
      console.warn(error);
      return `Er is een onbekende fout "${error}" opgetreden, neem contact op met uw beheerder`;
    }
  }
}

function apiV1Error(responseData) {
  return {
    type: get(responseData.result.instance, 'type'),
    requestID: get(responseData, 'request_id'),
  };
}
