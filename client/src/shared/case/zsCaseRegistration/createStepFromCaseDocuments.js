import angular from 'angular';
import capitalize from 'lodash/capitalize';
import propCheck from './../../util/propCheck';

export default ( caseDocuments ) => {

	propCheck.throw(
		propCheck.shape({
			caseDocuments: propCheck.arrayOf(
				propCheck.shape({
					name: propCheck.string,
					label: propCheck.string
				})
			)
		}),
		{
			caseDocuments
		}
	);

	return {
		name: 'file',
		label: 'Toevoegen document',
		icon: 'file',
		fieldsets: [
			{
				name: 'files',
				label: 'Toevoegen document',
				fields: [
					{
						name: '$files',
						template: 'form',
						data: {
							fields: [
								{
									name: 'name',
									template: {
										inherits: 'text',
										control: ( element ) => {

											return angular.element(
												`<div class="filename-control" ng-class="{'not-empty': !vm.$empty}">
													${element[0].outerHTML}
													<span class="file-extension">{{::vm.templateData().extension}}</span>
													<zs-icon ng-if="!vm.templateData().isArchivable" icon-type="alert-circle" zs-tooltip="Dit bestandstype kan niet worden gearchiveerd" zs-tooltip-options="{ attachment: 'right middle', target: 'left middle', flip: true, offset: { x: -10, y: 0 } }"></zs-icon>
												</div>`
											);

										},
										display: ( element ) => {

											return angular.element(
												`<div class="filename-control" ng-class="{'not-empty': !vm.$empty}">
													${element[0].outerHTML}
													<span class="file-extension">{{::vm.templateData().extension}}</span>
													<zs-icon ng-if="!vm.templateData().isArchivable" icon-type="alert-circle" zs-tooltip="Dit bestandstype kan niet worden gearchiveerd" zs-tooltip-options="{ attachment: 'right middle', target: 'left middle', flip: true, offset: { x: -10, y: 0 } }"></zs-icon>
												</div>`
											);

										}
									},
									label: 'Naam',
									required: true
								},
								{
									name: 'origin',
									template: 'select',
									label: 'Documentrichting',
									data: {
										options:
										'inkomend uitgaand intern'
											.split(' ')
											.map(capitalize)
											.map(
												value => {
													return { value, label: value };
												}
											)
									},
									required: true
								},
								{
									name: 'origin_date',
									template: 'date',
									label: 'Datum verzenden/ontvangen',
									data: {
										placeholder: 'Vul een datum in (dd-mm-jjjj)'
									},
									when: [ '$values', vals => vals.origin !== 'Intern' ],
									required: false
								},
								{
									name: 'description',
									template: 'text',
									label: 'Omschrijving',
									required: false
								},
								{
									name: 'case_document',
									template: 'select',
									label: 'Zaakdocument',
									data: {
										notSelectedLabel: 'Geen',
										options: caseDocuments.map(
											caseDoc => {
												return { label: caseDoc.label, value: caseDoc.name };
											}
										)
									},
									required: false,
									when: caseDocuments.length
								}
							]
						},
						limit: -1
					}
				]
			}
		]
	};

};
