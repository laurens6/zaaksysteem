import BaseReducer from './../resourceReducer/BaseReducer';

export default ( value ) => {

	let reducer = new BaseReducer();

	reducer.$setState('resolved');
	reducer.setSrc(value);

	return reducer;
};
