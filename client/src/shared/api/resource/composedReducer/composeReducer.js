import defaultsDeep from 'lodash/defaultsDeep';
import first from 'lodash/head';
import tail from 'lodash/tail';
import ApiResource from './../ApiResource';
import BaseReducer from './../resourceReducer/BaseReducer';
import ComposedReducer from './../resourceReducer/ComposedReducer';
import PromiseReducer from './../resourceReducer/PromiseReducer';
import createReducerFromFunction from './createReducerFromFunction';
import createReducerFromResource from './createReducerFromResource';
import createReducerFromValue from './createReducerFromValue';

/**
 * Helper function to create a ComposedReducer instance.
 * Takes an options argument and an arbitrary number of reducers
 * that are mapped to resource reducers by type.
 * ZS-TODO: research (E|J)SDoc documentation of ES2015 rest parameters
 *
 * @return {ComposedReducer}
 */
export default function composedReducer( ...rest ) {
	let options = first(rest);
	let children = tail(rest);

	if (typeof options === 'function' || (options instanceof BaseReducer)) {
		options = {};
		children = [options].concat(children);
	}

	options = defaultsDeep({
		eager: options.waitUntilResolved === false
	}, options);

	const reducer = new ComposedReducer(
		options,
		children.map(
			child => {
				if (child instanceof BaseReducer) {
					return child;
				}

				if (child instanceof ApiResource) {
					return createReducerFromResource(child);
				}

				if (typeof child === 'function') {
					return createReducerFromFunction(options.scope, child);
				}

				if (child && typeof child.then === 'function') {
					return new PromiseReducer(child);
				}

				return createReducerFromValue(child);
			}
		)
	);

	if (options.scope === undefined) {
		if (ENV.IS_DEV) {
			console.warn('Using a composedReducer without a scope is deprecated.');
			console.trace();
		}
	} else {
		options.scope.$on('$destroy', () => {
			reducer.destroy();
		});
	}

	reducer.data = () => reducer.value();

	reducer.onUpdate = ( fn, opts ) => {
		if (ENV.IS_DEV) {
			console.warn('ComposedReducer.onUpdate is deprecated. Please use ComposedReducer.subscribe()');
		}

		return reducer.subscribe(fn, opts);
	};

	if (options.scope && options.mode === 'hot') {
		options.scope.$watch(reducer.data);
	}

	return reducer;
}
