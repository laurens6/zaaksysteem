
BEGIN;

    CREATE TYPE documentstatus AS ENUM (
        'original',
        'copy',
        'replaced',
        'converted'
    );

    ALTER TABLE
        file
    ADD COLUMN
        document_status
        documentstatus
    DEFAULT
        'original'::documentstatus
    NOT NULL;

COMMIT;
