BEGIN;

    CREATE INDEX IF NOT EXISTS object_relation_object_id_idx ON object_relation(object_id);
    CREATE INDEX IF NOT EXISTS object_relation_object_uuid_idx ON object_relation(object_uuid);

COMMIT;
