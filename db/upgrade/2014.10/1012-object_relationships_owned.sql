BEGIN;

ALTER TABLE object_relationships ADD COLUMN owner_object_uuid UUID REFERENCES object_data (uuid) ON DELETE CASCADE;

COMMIT;
