/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.filters')
		.factory('sizeFilter', [ '$filter', function ( $filter ) {
			
			var numberFilter = $filter('number');
			
			return function ( from, fractionSize ) {
				var num, append;
				from = Number(from);
				if(from < 1024) {
					num = from;
					append = 'bytes';
				} else if(from < 1024 * 1024) {
					num = from/1024;
					append = 'KB';
				} else if(from >= 1024 * 1024) {
					num = from/1024/1024;
					append = 'MB';
				}
				return numberFilter(num, fractionSize) + ' ' + append;
			};
			
			
		}]);
	
})();
