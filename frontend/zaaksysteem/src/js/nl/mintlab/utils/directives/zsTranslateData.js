/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.locale')
		.directive('script', [ 'translationService', function ( translationService ) {
			
			return {
				restrict: 'E',
				scope: true,
				terminal: true,
				compile: function ( /*tElement, tAttrs, transclude*/ ) {
					
					return function link ( scope, element, attrs ) {
						
						if(attrs.type === 'text/zs-translation-data') {
							var data;
						
							try {
								data = JSON.parse(element[0].innerHTML);
							} catch ( error ) {
								console.log('Error parsing translation data: ' + error);
							}
							
							for(var key in data) {
								translationService.set(key, data[key]);
							}
						}
					};
				}
			};
		}]);
})();