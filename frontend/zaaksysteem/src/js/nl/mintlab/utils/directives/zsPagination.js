/*global angular,_*/
angular.module('Zaaksysteem')
	.directive('zsPagination', [ function ( ) {

		return {
			restrict: 'A',
			scope: {
				'currentPage': '@zsPaginationCurrent',
				'numPages': '@zsPaginationTotal',
				'numRows': '@zsPaginationNumRows',
				'loading': '@zsPaginationLoading'
			},
			templateUrl: '/html/directives/pagination/pagination.html',
			compile: function ( ) {

				return function link ( scope/*, element, attrs*/ ) {
					
					scope.getAvailablePages = function ( ) {
						var numPages = parseInt(scope.numPages, 10);
						var currentPage = parseInt(scope.currentPage, 10);
						var pages = [ 1, currentPage, numPages ];

						pages = _.unique(_.filter(pages,
									function ( num ) {
										return num >= 1 && num <= numPages;
									}));

						return pages;
					};

					scope.getCurrentPage = function ( ) {
						return parseInt(scope.currentPage, 10);
					};

					scope.getNumPages = function ( ) {
						return parseInt(scope.numPages, 10);
					};

					scope.getLoading = function ( ) {
						return scope.loading === 'true';
					};

					scope.showPage = function ( page ) {
						scope.$emit('zs.pagination.page.change', page);
					};

					scope.hasPage = function ( page ) {
						return _.indexOf(scope.getAvailablePages(), page) !== -1;
					};

					scope.onNumRowsChange = function ( ) {
						scope.$emit('zs.pagination.numrows.change', Number(scope.numRows));
					};
				};

			}

		};

	}]);
