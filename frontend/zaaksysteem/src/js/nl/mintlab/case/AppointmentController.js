 /*global angular,fetch*/
(function () {
	"use strict";

	angular.module('Zaaksysteem.case')
		.controller('nl.mintlab.case.AppointmentController', [ '$scope', 'smartHttp', 'translationService', 'dateFilter', function ($scope, smartHttp, translationService, dateFilter ) {

			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

			$scope.dates = [];
			$scope.times = [];

			function getDates ( ) {
				smartHttp.connect({
					method: 'GET',
					url: '/api/appointment/get_dates',
					params: {
						interface_uuid: $scope.interface_uuid,
						product_id: $scope.product_id,
						location_id: $scope.location_id,
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						$scope.dates = response.result;
					})
					.error(function ( response ) {
						broadcastError(response);
					});
			}

			function getTimes ( ) {
				smartHttp.connect({
					method: 'GET',
					url: '/api/appointment/get_timeslots',
					params: {
						interface_uuid: $scope.interface_uuid,
						date: $scope.appointmentDate,
						product_id: $scope.product_id,
						location_id: $scope.location_id,
						zapi_no_pager: 1
					}
				})
					.success(function ( response ) {
						$scope.times = response.result;

					})
					.error(function ( response ) {
						broadcastError(response);
					});
			}
			
			function broadcastError ( response ) {
				var message = {
						type: 'error',
						content: translationService.get('Er ging iets fout bij het ophalen van de beschikbare data. Probeer het later opnieuw.')
					},
					type = response.result && response.result[0] ? response.result[0].type : null;
				
				if(type === 'appointment/config_missing') {
					message.content = translationService.get('Er is nog geen afsprakenkoppeling geconfigureerd.');
				}
				
				$scope.$emit('systemMessage', message);
			}

			function updateZSCalendarAttribute() {

				var attributeName = $scope.input_field_name,
					changes = {
						update: attributeName
					}

				changes[attributeName] = $scope.appointment;

				smartHttp.connect({
					method: 'POST',
					url: '/pip/zaak/' + $scope.$parent.caseId + '/request_attribute_update/' + $scope.$parent.bibliotheekId,
					data: changes
				})
					.success(function ( response ) {
						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Uw afspraak is geboekt op ' + dateFilter($scope.appointmentSlot.start_time, 'longDate') + ' om ' + dateFilter($scope.appointmentSlot.start_time, 'shortTime'))
						});

					})
					.error(function ( /*response*/ ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het opslaan van uw wijziging. Probeer het later opnieuw')
						});
					});

			}

			$scope.setAppointmentTime = function ( slot ) {
		$scope.appointmentSlot = slot;
			};

			$scope.setAppointmentDate = function ( date ) {
				$scope.appointmentDate = date;
			};

			$scope.deleteAppointment = function () {
				smartHttp.connect({
					method: 'POST',
					url: '/api/appointment/delete_appointment',
					params: {
						interfaceUuid: $scope.interface_uuid,
						appointment: $scope.appointment
					}
				})
					.success(function ( /*response*/ ) {

						$scope.confirmedSlot = null;
						$scope.appointment = null;

						if ($scope.pip) {

							updateZSCalendarAttribute();

						}

						$scope.$emit('systemMessage', {
							type: 'info',
							content: translationService.get('Uw afspraak is verwijderd')
						});
					})
					.error(function ( /*response*/ ) {

					});

			};

			$scope.book = function ( ) {
				smartHttp.connect({
					method: 'POST',
					url: '/api/appointment/book_appointment',
					headers: {
						"Content-Type": "application/json"
					},
					data: JSON.stringify({
						appointment: $scope.appointmentSlot
					})
				})
					.success(function ( response ) {

						$scope.confirmedSlot = $scope.appointmentSlot;
						$scope.appointment = response.result[0].id;

						// When we are on the PIP, we want to skip the extra confirmation step, and immediately update the attribute on ZS.

						if ($scope.pip) {

							updateZSCalendarAttribute();

						} else {
							$scope.$emit('systemMessage', {
								type: 'info',
								content: translationService.get('Uw afspraak is geboekt op ' + dateFilter($scope.appointmentSlot.start_time, 'longDate') + ' om ' + dateFilter($scope.appointmentSlot.start_time, 'shortTime'))
							});
						}


						$scope.closePopup();
					})
					.error(function ( /*response*/ ) {

					});
			};
			
			$scope.getTimeLabel = function ( slot ) {
				var start = new Date(slot.start_time).getTime(),
					finish = new Date(slot.end_time).getTime(),
					from,
					to;
					
				if(isNaN(start)) { 
					// we're dealing with an old browser here,
					// unable to parse iso formats
					start  = new Date(slot.start_time.replace(/-/g, '/').replace('00.000', '').replace(/(.*):/, '$1')).getTime();
					finish = new Date(slot.end_time.replace(/-/g, '/').replace('00.000', '').replace(/(.*):/, '$1')).getTime();
				}
					
				from = dateFilter(start, 'HH:mm');
				to = dateFilter(finish, 'HH:mm');
					
				return from + ' - ' + to;
			};

			$scope.retrieveAppointment = function ( ) {
				if (!$scope.appointment) { return; }

				smartHttp.connect({
					method: 'GET',
					url: '/api/appointment/get_appointment',
					params: {
						appointment_id: $scope.appointment
					}
				})
					.success(function ( response ) {
						$scope.confirmedSlot = response.result[0];
					})
					.error(function ( /*response*/ ) {

					});
			};

			$scope.$on('popupopen', function ( /*event*/ ) {
				
				$scope.appointmentDate = null;
				$scope.appointmentSlot = null;
				
				safeApply($scope, function ( ) {
					if ($scope.interface_uuid) {
						getDates();
					} else {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er is iets misgegaan. Neem contact op met de gemeente.')
						});
						$scope.closePopup();
					}
				});
			});

			$scope.$watch('appointmentDate', function ( ) {
				if($scope.appointmentDate) {
					getTimes();
				} else {
					$scope.times = [];
				}
			});

		}]);
})();
