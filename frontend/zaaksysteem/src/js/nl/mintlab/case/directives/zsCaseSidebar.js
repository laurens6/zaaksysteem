/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseSidebar', [ function ( ) {
			
			return {
				require: [ 'zsCaseSidebar', '^zsCaseView', '^zsCasePhaseView' ],
				controller: [ '$scope', function ( $scope ) {
					
					var ctrl = this,	
						zsCaseView,
						zsCasePhaseView;
					
					ctrl.link = function ( controllers ) {
						zsCaseView = controllers[0];
						zsCasePhaseView = controllers[1];
					};
					
					ctrl.getChecklistItems = function ( ) {
						var milestone = zsCasePhaseView.getMilestone(),
							caseObj,
							checklistItems = null;
							
						if(zsCaseView.isLoaded()) {
							caseObj = zsCaseView.getCase();
							checklistItems = caseObj['case'].checklist.by_milestone[milestone] || [];
						}
						
						return checklistItems;
					};
					
					ctrl.getActions = function ( ) {
						var loaded = zsCaseView ? zsCaseView.isLoaded() : false,
							caseObj,
							actions = null;
							
						if(loaded) {
							caseObj = zsCaseView.getCase();
							actions = _.reject(caseObj['case'].case_actions.by_milestone[zsCasePhaseView.getMilestone()] || [], { type: 'object_mutation' });
						}
							
						return actions;	
					};
					
					ctrl.getCaseDocuments = function ( ) {
						var loaded = zsCaseView ? zsCaseView.isLoaded() : false,
							documents;	
							
						if(loaded) {
							documents = zsCaseView.getCase()['case'].case_documents;
						}
							
						return documents;
					};
					
					ctrl.isClosed = function ( ) {
						return $scope.closed;
					};
					
					
					return ctrl;
				}],
				controllerAs: 'caseSidebar',
				link: function ( scope, element, attrs, controllers  ) {
					
					controllers[0].link(controllers.slice(1));
					
					scope.getCaseDocs = function ( ) {
						return controllers[0].getCaseDocuments();	
					};
					
					
				}
			};
			
		}]);
	
})();
