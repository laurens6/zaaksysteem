/*global angular*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseWebformObjectTypeFormAdd', [ function ( ) {
			
			return {
				require: [ 'zsCaseWebformObjectTypeFormAdd', 'zsCaseWebformObjectTypeForm', '^zsCaseWebformObjectTypeMutationAdd' ],
				controller: [ function ( ) {
					
					var ctrl = this,
						zsCaseWebformObjectTypeForm,
						zsCaseWebformObjectTypeMutationAdd;
					
					ctrl.setControls = function ( ) {
						zsCaseWebformObjectTypeForm = arguments[0];
						zsCaseWebformObjectTypeMutationAdd = arguments[1];
						zsCaseWebformObjectTypeMutationAdd.setForm(zsCaseWebformObjectTypeForm);
					};
					
					return ctrl;
					
				}],
				controllerAs: 'caseWebformObjectTypeFormAdd',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControls.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
