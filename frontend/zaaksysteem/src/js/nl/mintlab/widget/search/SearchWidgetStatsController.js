(function ( ) {
	
	angular.module('Zaaksysteem.widget.search')
		.controller('nl.mintlab.widget.search.SearchWidgetStatsController', [ '$scope', '$http', 'systemMessageService', function ( $scope, $http, systemMessageService ) {
			
			var ctrl = this,
				loading = true,
				count = NaN,
				where;
			
			ctrl.isLoading = function ( ) {
				return loading;	
			};
			
			ctrl.getCount = function ( ) {
				var label = count;
				if(isNaN(count)) {
					label = '-';
				}
				return label;
			};
			
			where = $scope.getZqlWhere() || '';
			if(where) {
				where = ' WHERE ' + where;
			}
			
			$http({
				method: 'GET',
				url: '/api/object/search',
				params: {
					zql: 'COUNT ' + $scope.getObjectType().object_type + where
				}
			})
				.success(function ( response ) {
					count = response.result[0].count;
				})
				.error(function ( /*response*/ ) {
					systemMessageService.emitLoadError('statistieken');
				})
				['finally'](function ( ) {
					loading = false;
				});
			
		}]);
	
})();
