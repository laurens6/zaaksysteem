/*global angular,fetch,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.pip')
		.controller('nl.mintlab.pip.PipDocumentController', [ '$scope', 'translationService', 'smartHttp', function ( $scope, translationService, smartHttp ) {
			
			$scope.documents = null;
			
			var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
				queue = [];
				
			function reloadData ( ) {
				smartHttp.connect({
					url: '/pip/zaak/' + $scope.caseId + '/file/search',
					method: 'GET'
				})
					.success(function ( response ) {
						$scope.documents = response.result;
					})
					.error(function ( ) {
						$scope.$emit('systemMessage', {
							type: 'error',
							content: translationService.get('Er ging iets fout bij het ophalen van de documenten. Probeer het later opnieuw.')
						});
					});
			}
			
			$scope.getTotalProgress = function ( ) {
				var loadedBytes = 0,
					totalBytes = 0;
				
				_.each(queue, function ( upload ) {
					totalBytes += upload.totalBytes || 0;
					loadedBytes += upload.loadedBytes || 0;
				});
				
				return loadedBytes/totalBytes;
			};
			
			$scope.getQueue = function ( ) {
				return queue;
			};
			
			$scope.clearQueue = function ( ) {
				queue.length = 0;	
			};
			
			$scope.$on('upload.start', function ( event, upload ) {
				safeApply($scope, function ( ) {
					queue.push(upload);	
				});
			});
			
			$scope.$on('upload.end', function ( event, upload ) {
				safeApply($scope, function ( ) {
					
				});
			});
			
			$scope.$on('upload.progress', function ( event, upload ) {
				safeApply($scope, function ( ) {
					
				});
			});
			
			$scope.$on('upload.complete', function ( event, upload ) {
				if (upload.error) {
					var response = JSON.parse(upload.xhr.response);
					$scope.$emit('systemMessage', {
						type: 'error',
						content: translationService.get(response.result[0].messages.join(", "))
					});
				} else {
					var file = upload.getData().result[0];
					$scope.documents.push(file);
				}
			});
			
			$scope.$watch('caseId', function ( ) {
				if($scope.caseId) {
					reloadData();
				}
			});
				
		}]);
		
})();
