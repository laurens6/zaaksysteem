package Zaaksysteem::Test::Backend::Sysin::Interface::Component;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Backend::Sysin::Interface::Component;

sub _get_fake_self {
    return mock_strict(
        result_source => { schema => mock_dbix_schema(is_multi_tenant => 1) },
        @_,
    );
}

sub test_multi_tenant {
    my $does = 0;
    my $fake_self = _get_fake_self(
        module_object => { does => sub { return  $does }});
    ok(
        !Zaaksysteem::Backend::Sysin::Interface::Component::is_multi_tenant(
            $fake_self),
        "Doesn't role with multiples"
    );

    $does = 1;
    ok(
        Zaaksysteem::Backend::Sysin::Interface::Component::is_multi_tenant(
            $fake_self),
        "Does role multi-tenant"
    );
}

sub test_supports_hostname {
    my $mt = 0;
    my %interface_config;
    my $fake_self = _get_fake_self(
        is_multi_tenant      => sub { return $mt },
        get_interface_config => sub { return \%interface_config }
    );

    ok(
        Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo"),
        "Supports all the hostnames if interface does 'ZS::Backend::Sysin::Modules::Roles::MT'"
    );

    $mt = 1;

    ok(
        Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo"),
        "Supports all the hostnames with MT enabled"
    );

    ok(
        !Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo", 1),
        "... but not in exact mode"
    );

    $interface_config{is_multi_tenant} = 0;

    ok(
        Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo"),
        "Supports the hostname in MT catch all mode"
    );

    ok(
        !Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo", 1),
        "... unless we search on exact hostnames"
    );

    $interface_config{is_multi_tenant}   = 1;
    $interface_config{multi_tenant_host} = "Foo";

    ok(
        Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo"),
        "Supports only 'Foo' in MT mode"
    );

    ok(
        Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Foo", 1),
        "... also in exact mode"
    );

    ok(
        !Zaaksysteem::Backend::Sysin::Interface::Component::supports_hostname(
            $fake_self, "Bar", 1),
        "... but not 'Bar'"
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Interface::Component - An interface resultset tester

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Backend::Sysin::Interface::Component

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
