package Zaaksysteem::Object::Types::Group;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';

use BTTW::Tools;
use Zaaksysteem::Types qw(NonEmptyStr);

=head1 NAME

Zaaksysteem::Object::Types::Group - Built-in type to represent "groups" (organizational units)

=head1 DESCRIPTION

Groups (organizational units) are used in assigning cases to people.

=head1 ATTRIBUTES

=head2 group_id

The "legacy" group ID.

=cut

has group_id => (
    is => 'ro',
    isa => 'Int',
    traits => [qw(OA)],
    label => 'Group ID',
    required => 1,
    unique => 1,
);

=head2 name

The name of the group

=cut

has name => (
    is => 'ro',
    isa => NonEmptyStr,
    traits => [qw(OA)],
    label => 'Group name',
    required => 1,
    unique => 1,
);

=head2 description

A short description of the group

=cut

has description => (
    is => 'ro',
    isa => 'Str',
    traits => [qw(OA)],
    label => 'Group description',
    required => 1,
);

=head1 METHODS

=head2 TO_STRING

Overrides L<Zaaksysteem::Object/TO_STRING>.

=cut

override TO_STRING => sub {
    return shift->name;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
