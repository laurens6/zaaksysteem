package Zaaksysteem::Tools::X509;
use strict;
use warnings;
use Carp qw(croak);

use Exporter qw(import);

our @EXPORT_OK = qw(
    split_chain
);

=head1 NAME

Zaaksysteem::Tools::X509 - An X509 toolset

=head1 DESCRIPTION

Handy tools for using X509 certs in Zaaksysteem

=head1 SYNOPSIS

    use Zaaksysteem::Tools::X509 qw(split_chain)

    my @certs = split_chain($chained_cert);

=head1 METHODS

=head2 split_chain

Split a certificate chain in multiple chains

=cut

sub split_chain {
    my $chain = shift;

    my $fh;
    if (ref $chain) {
        $fh = $chain;
    }
    else {
        use autodie;
        open($fh, '<', $chain);
    }

    my ($line, $is_cert, @chain, @cert);

    while (defined($line = $fh->getline())) {
        if ($line eq "-----BEGIN CERTIFICATE-----\n") {
            croak("Invalid BEGIN certificate found!") if $is_cert;
            $is_cert = 1;
            @cert = $line;
        }
        elsif ($line eq "-----END CERTIFICATE-----\n") {
            croak("Invalid END certificate found!") unless $is_cert;
            push(@chain, join("", @cert, $line));
            @cert = ();
            $is_cert = 0;
        }
        elsif ($is_cert) {
            push(@cert, $line);
        }
    }

    croak("No certificate found?") unless @chain;

    return @chain unless $is_cert;

    croak(sprintf("No END found after %d chains", scalar @chain));
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
