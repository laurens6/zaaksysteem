package Zaaksysteem::DB::Component::Logging::Event;
use Moose::Role;

use BTTW::Tools;
use Encode qw(encode_utf8);
use JSON;
use PerlX::Maybe::XS qw(provided);
use Zaaksysteem::Object::Types::Event;
use Zaaksysteem::BR::Subject;

has _onderwerp => (
    is => 'rw',
    isa => 'Bool',
);

has _subject_bridge => (
    is      => 'ro',
    isa     => 'Zaaksysteem::BR::Subject',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Zaaksysteem::BR::Subject->new(
            schema => $self->result_source->schema,);
    }
);

=head2 data

This method is a backwards compatible proxy for C<event_data>.

=cut

sub data {
    return shift->event_data(@_) || {};
}

before [qw[update insert]] => sub {
    my $self = shift;

    # Automagic for a sort-of required field. At least all events will show up in the logbook now
    unless(defined $self->component) {
        $self->component('event');
    }

    $self->set_column(onderwerp => $self->onderwerp);
};

# Placeholder to be overwritten by event roles that actually
# define some additional attributes
sub _add_magic_attributes { }

# Most generic kind of subjectline available for events
sub onderwerp {
    my ($self) = @_;

    $self->_onderwerp(1);

    return sprintf('Event "%s": %s', $self->event_type, dump_terse($self->event_data));
}

# Allow inflection for role package names derived from the event type of the auditlog event bound to this role.
# in goes 'kcc/call', out comes 'Kcc::Call'. Supports many levels of nesting.
sub inflect_event_type {
    my ($self, $event_type) = @_;

    return unless defined $event_type;

    return sprintf(
        'Zaaksysteem::DB::Component::Logging::%s',
        join('::', map { join('', map(ucfirst, split('_', $_))) } split('/', $event_type))
    );
}

sub event_category {
    my @type_split = split m[/], shift->event_type;

    return shift @type_split;
}

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    for my $key (keys %{ $self->data }) {
        $data->{ $key } = $self->data->{ $key };
    }

    return $data;
};



sub as_object {
    my $self = shift;

    my $event_data = $self->data;
    unless ($self->_onderwerp) {
        $event_data->{human_readable} = $self->get_column('onderwerp') // $self->onderwerp;
    }

    my %people;
    foreach my $i (qw(created_by created_for modified_by)) {
        try {
            $people{$i} = $self->_subject_bridge->get_by_old_subject_identifier($self->$i) if $self->$i;
        }
        catch {
            $self->log->debug(
                sprintf(
                    "Unable to translate '%s' to a subject: %s",
                    $self->$i, $_
                )
            );
        };
    }

    my $case_id = $self->get_column('zaak_id');
    my $event = Zaaksysteem::Object::Types::Event->new(
        event_type  => $self->event_type,
        event_data  => $event_data,
        event_id    => $self->id,
        component   => $self->component,

        date_created  => $self->created,
        date_modified => $self->last_modified,

        %people,

        provided $case_id, case_id => $case_id,
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 encode

TODO: Fix the POD

=cut

=head2 event_category

TODO: Fix the POD

=cut

=head2 inflect_event_type

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

