package Zaaksysteem::DB::Component::Logging::Object::Relation;
use Moose::Role;

use HTML::Entities qw(encode_entities);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Object::Relation - Event subject for object relation changes

=head1 METHODS

=head2 onderwerp

Overrides L<Zaaksysteem::Schema::Logging/onderwerp> and provides a
human-readable summary of the event.

=cut

sub onderwerp {
    my $self = shift;

    my ($action) = (split('/', $self->event_type))[-1];

    if ($action eq 'add') {
        $action = 'gelegd';
    }
    elsif ($action eq 'delete') {
        $action = 'verbroken';
    }

    return sprintf(
        'Relatie met %s "%s" %s',
        $self->data->{related_object_type_name},
        $self->related_object_url,
        $action,
    );
}

=head2 related_object_url

This method uses data from the event to attempt to construct an html string
with a hyperlink to the object found therein.

=cut

sub related_object_url {
    my $self = shift;

    my $uuid  = $self->data->{related_object_id};
    my $type  = $self->data->{related_object_type};
    my $label = $self->data->{related_object_label};

    unless($uuid) {
        return sprintf(
            '<span style="text-decoration: line-through" title="Object is niet meer beschikbaar">%s</span>',
            encode_entities($label),
        );
    }

    if(exists $self->data->{related_case_id}) {
        return sprintf(
            '<a target="_top" href="/zaak/%d">%s</a>',
            $self->data->{related_case_id},
            encode_entities($label)
        );
    }

    return sprintf(
        '<a target="_top" href="/object/%s">%s</a>',
        $uuid,
        encode_entities($label)
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
