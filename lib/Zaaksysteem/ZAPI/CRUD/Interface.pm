package Zaaksysteem::ZAPI::CRUD::Interface;

use Moose;

use Zaaksysteem::ZAPI::CRUD::Interface::Column;
use Zaaksysteem::ZAPI::CRUD::Interface::Action;

use constant OBJECT_PROFILE     => {
    required            => [qw/
        columns
        url
    /],
    optional            => [qw/
        actions
        options
        filters
        style
    /],
    constraint_methods  => {
        columns         => sub {
            my ($dfv)   = @_;

            my $val     = $dfv->get_filtered_data->{
                $dfv->get_current_constraint_field
            };

            return unless UNIVERSAL::isa($val, 'ARRAY');

            return unless scalar @{ $val };

            for my $item (@{ $val }) {
                return unless UNIVERSAL::isa(
                    $item,
                    'Zaaksysteem::ZAPI::CRUD::Interface::Column'
                );
            }

            return 1;
        },
        actions         => sub {
            my ($dfv)   = @_;

            my $val     = $dfv->get_filtered_data->{
                $dfv->get_current_constraint_field
            };

            return unless UNIVERSAL::isa($val, 'ARRAY');

            return unless scalar @{ $val };

            for my $item (@{ $val }) {
                return unless UNIVERSAL::isa(
                    $item,
                    'Zaaksysteem::ZAPI::CRUD::Interface::Action'
                );
            }

            return 1;
        },
        options        => sub {
            my ($dfv)   = @_;

            my $val     = $dfv->get_filtered_data->{
                $dfv->get_current_constraint_field
            };

            return unless UNIVERSAL::isa($val, 'HASH');

            return 1;
        },
    },
};

=head1 NAME

Zaaksysteem::ZAPI::CRUD::Interface - Generate a CRUD interface instruction for
Angular

=head1 SYNOPSIS

    $interface = Zaaksysteem::ZAPI::CRUD::Interface->new(
        columns     => [
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id      => 'external_transaction_id',
                label   => 'External id',
                resolve => 'external_transaction_id'
            ),
            Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
                id          => 'date_created',
                label       => 'datum/tijd',
                resolve     => 'date_created',
                sort        => {
                    type        => 'alphanumeric',
                    resolve     => 'date_created'
                }
            ),
        ],
        actions    => [],
        filters    => [],
        options    => {
            select => 'all',
        },
        style      => {}
    )


=head1 DESCRIPTION

The CRUD Interface description for Angular

=head1 ATTRIBUTES

=head2 columns [required]

ISA: Array of L<Zaaksysteem::ZAPI::CRUD::Interface::Column>

 $interface->columns([
    Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id      => 'external_transaction_id',
        label   => 'External id',
        resolve => 'external_transaction_id'
    ),
    Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
        id          => 'date_created',
        label       => 'datum/tijd',
        resolve     => 'date_created',
        sort        => {
            type        => 'alphanumeric',
            resolve     => 'date_created'
        }
    ),
 ])

Definition of every column for this CRUD table

=cut

has 'columns'    => (
    is          => 'ro',
    isa         => 'ArrayRef',
    required    => 1,
    default     => sub { return []; }
);

=head2 actions [optional]

ISA: Array of L<Zaaksysteem::ZAPI::CRUD::Interface::Action>

Definition of every action for this CRUD table

=head2 options [optional]

ISA: HashRef with options

    {
        select  => 'all',
    }

Defining the capabilities and their options for this CRUD table

B<Options>

=over 4

=item select [optional]

Value: one of C<all>, C<single>??

=back

=cut

has 'actions'    => (
    is          => 'ro',
    isa         => 'ArrayRef',
    default     => sub { return []; }

);

=head2 filters [optional]

ISA: Array of L<Zaaksysteem::ZAPI::CRUD::Interface::Filter>

Definition of every filter for this CRUD table

=cut

has 'filters'    => (
    is          => 'ro',
    isa         => 'ArrayRef',
    default     => sub { return []; }

);

=head2 options

ISA: HashRef

 {
    select  => 'all',
 }

A hashref with options for the Angular CRUD interface

=cut

has 'options'    => (
    is          => 'ro',
    isa         => 'HashRef',
    lazy        => 1,
    default     => sub { return {}; }
);

=head2 url

ISA: String

 $crudinterface->url($c->uri_for);

Will set the url for this crud platform, to use as default prefix

=cut

has 'url'    => (
    is          => 'rw',
    isa         => 'Str',
);

=head2 style
ISA: HashRef
 {
    select  => 'all',
 }

A hashref which contains styling data.

=cut

has 'style'     => (
    is          => 'ro',
    isa         => 'HashRef',
    default     => sub { return {}; }
);


=head1 METHODS

=head2 TO_JSON

Send data in json format

=cut

sub TO_JSON {
    my $self        = shift;

    $self->_validate;

    return $self->_object_params;
}


=head2 from_catalyst

Special helper function for usage of the ZAPI module within Catalyst, it will
set the different urls etc correct.

=cut

sub from_catalyst {
    my $self    = shift;
    my $c       = shift;

    $self->url(
        $c->uri_for(
            $c->action, $c->req->captures, @{ $c->req->args }
        )->as_string
    );

    return $self;
}


=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ OBJECT_PROFILE->{required} },
        @{ OBJECT_PROFILE->{optional} }
    ) };
}

=head2 _validate

Validates this object, to prove it is complete and contains valid attributes

=cut

sub _validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        OBJECT_PROFILE
    );

    die(
        'Cannot validate form field, invalid or missing: '
        . join(',', $dv->invalid, $dv->missing)
    ) unless $dv->success;

    return 1;
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 OBJECT_PROFILE

TODO: Fix the POD

=cut

