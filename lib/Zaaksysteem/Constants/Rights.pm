package Zaaksysteem::Constants::Rights;

use warnings;
use strict;

=head1 NAME

Zaaksysteem::Constants::Rights - Constants for user rights

=head1 DESCRIPTION

This module defined constants for user rights. Currently only does:

=over

=item C<USER>

Regular user

=item C<DASHBOARD>

User gets to see a dashboard

=back

=head1 SYNOPSIS

    use Zaaksysteem::Constants::Rights qw(PIP API REGULAR FORM);

    # Check if the user is logged in or not
    $c->assert_user(PIP);

    # Check if the user comes from API context, and has admin permissions
    $c->assert_user(API | ADMIN);

=cut

use Exporter qw[import];

our @EXPORT_OK  = qw(USER DASHBOARD SEARCH);
our %EXPORT_TAGS  = ( all => \@EXPORT_OK );

use constant USER      => 'user';
use constant DASHBOARD => 'dashboard';
use constant SEARCH    => 'search';

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
