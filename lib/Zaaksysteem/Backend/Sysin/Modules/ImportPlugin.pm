package Zaaksysteem::Backend::Sysin::Modules::ImportPlugin;

use Moose;

use Zaaksysteem::ZAPI::Form;
use Zaaksysteem::ZAPI::Form::Field;

use Zaaksysteem::Backend::Sysin::Transaction::Mutation;

extends 'Zaaksysteem::Backend::Sysin::Modules';


=head1 Interface Properties

Below a list of interface properties, see
L<Zaaksysteem::Backend::Sysin::Modules> for details.

=head2 INTERFACE_ID

The module ID for this interface

=cut

use constant INTERFACE_ID               => 'importplugin';

=head2 INTERFACE_CONFIG_FIELDS

Definition of this module

=cut

use constant INTERFACE_CONFIG_FIELDS    => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_gba_enabled',
        type => 'checkbox',
        required => 0,
        label => 'Activeer GBA CSV Plugin',
        description => 'Wanneer u gebruikmaakt van de verouderde GBA-CSV importmodule, dan kunt u deze hier aanzetten'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gba_module',
        type        => 'select',
        label       => 'GBA Module',
        data        => {
            options     => [
                {
                    value    => 'DeBiltCsv',
                    label    => 'De Bilt',
                },
                {
                    value    => 'BussumCsv',
                    label    => 'Bussum',
                },
            ],
        },
        required    => 0,
        description => 'Selecteer de GBA module voor uw organisatie, dit zorgt ervoor dat uw CSV op de juiste manier wordt geimporteerd'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_gba_filename',
        type        => 'text',
        label       => 'Filename voor gba file',
        required    => 0,
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name => 'interface_bag_enabled',
        type => 'checkbox',
        required => 0,
        label => 'Activeer BAG CSV Plugin',
        description => 'Wanneer u gebruikmaakt van de verouderde BAg-CSV importmodule, dan kunt u deze hier aanzetten'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_bag_module',
        type        => 'select',
        label       => 'GBA Module',
        data        => {
            options     => [
                {
                    value    => 'ImportCentric',
                    label    => 'De Bilt (Centric)',
                },
            ],
        },
        required    => 0,
        description => 'Selecteer de BAG module voor uw organisatie, dit zorgt ervoor dat uw CSV op de juiste manier wordt geimporteerd'
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_bag_filename',
        type        => 'text',
        label       => 'Filename voor bag file',
        required    => 0,
    ),
];


=head2 MODULE_SETTINGS

Settings for this module

=cut


use constant MODULE_SETTINGS => {
    name => INTERFACE_ID,
    label => 'Verouderde importplugin',
    interface_config => INTERFACE_CONFIG_FIELDS,
    direction => 'incoming',
    manual_type => [],
    is_multiple => 1,
    is_manual => 1,
    retry_on_error => 0,
    allow_multiple_configurations => 1,
    is_casetype_interface => 0,
};

around BUILDARGS => sub {
    my $orig = shift;
    my $class = shift;

    return $class->$orig( %{ MODULE_SETTINGS() }, @_ );
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


