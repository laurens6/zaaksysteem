/*global $,Highcharts,document,console*/
(function () {
    'use strict';

    $(document).ready(function () {

        if ($('#ezra_search_chart').length === 0) {
            return;
        }

        // privates
        var selector = $('.chart_profile_selector select#chart_profile'),
            container = $('#search_query_chart_container');

        function render(chart_data) {
            if (!chart_data.chart) {
                chart_data.chart = {};
            }

            chart_data.chart.renderTo = 'search_query_chart_container';

            //see how wide the chart is, so the exported graph will look the same
            var chart_width = container.width();
            chart_data.exporting = { width : chart_width };

            if (selector.val() === 'afhandeltermijn') {
                chart_data.tooltip = {
                    formatter: function () {
                        return '<b>' + this.point.name + '</b>: ' + this.y + ' %';
                    }
                };
            }


            if (chart_data.series) {
                container.highcharts(chart_data);
            } else {
                container.html('Geen resultaten');
            }
        }

        function sourceUrl() {
            var url,
                form = container.closest('form'),
                searchQueryId;

            searchQueryId = form.find('input[name=SearchQuery_search_query_id]').val();

            url = searchQueryId ? "/search/" + searchQueryId + "/results/chart" : "/search/results/chart";
            url += "?" + form.serialize();

            // needs to be done after serialize
            selector.attr('disabled', 'disabled');

            return url;
        }

        function load() {
            // highcharts will replace the inner html of container, so 
            // we become a monster so the monster will not break us
            container.html('<div class="spinner-groot"><div></div></div>');
            container.find('.spinner-groot').css('visibility', 'visible');

            $.getJSON(sourceUrl(), function (data) {
                if (!data.json) {
                    console.log("no data received");
                    return;
                }
                console.log(data);
                render(data.json);
                selector.attr('disabled', null);
            });
        }

        container.each(function () {
            load();
        });

        // event handler
        selector.change(function () {
            load();
        });
    });
}());
