#!/usr/bin/env perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI;
use BTTW::Tools;

my $cli     = Zaaksysteem::CLI->init();
my $options = $cli->options;

my $cat = delete $options->{category};

if (!$cat) {
    die "Unable to process create kenmerk without a category";
}

foreach (keys %$options) {
    create_kenmerk(
        bibliotheek_categorie_id => $cat,
        naam                     => $_,
        value_type               => $options->{$_},
    );

}

sub create_kenmerk {
    my %params = @_;

    $params{magic_string} //= $params{naam};
    $params{value_type} //= 'text';

    my $rs = $cli->schema->resultset("BibliotheekKenmerken");
    if ($rs->search_rs({ magic_string => $params{magic_string}, deleted => undef })->first) {
        $cli->log->error("$params{magic_string} already exists!");
        return;
    }

    return $cli->do_transaction(
        sub {
            my $bk = $cli->schema->resultset("BibliotheekKenmerken")->create(
                {
                    %params,
                    help        => $params{naam},
                    description => $params{naam},
                }
            );
            $cli->log->info(sprintf("Created bibliotheekkenmerk '%s' with id %d", $params{naam}, $bk->id));
            return $bk;
        }
    );
}

1;

__END__

=head1 NAME

create_kenmerken.pl - Create attributes via the CLI

=head1 SYNOPSIS

    ./dev-bin/create_kenmerken --o category=22 -o name=type

Eg, you want an attribute called test_like_attribute as text:

    ./dev-bin/create_kenmerken --o category=22 -o text_like_attribute=text


Create kenmerken, the name of an attribute is also the magic_string.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
