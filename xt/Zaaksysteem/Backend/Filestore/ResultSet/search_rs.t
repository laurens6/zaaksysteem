use Test::Most;

use Test::DBIx::Class { schema_class => 'Zaaksysteem::Schema' };

ok ResultSet('Filestore'), "ResultSet OK: Filestore";

my $resultset = ResultSet('Filestore')
    ->search( { uuid => 'ed34f2b9-f793-4f04-ab72-95e7bfff136c' } );

cmp_deeply (
    $resultset->{'cond'} => superhashof (
        {
            uuid => 'ed34f2b9-f793-4f04-ab72-95e7bfff136c',
            virus_scan_status => {
                -not_like => 'found%', # found: virus_name
            }
        }
    ),
    'search gets limited by default virus_scan_status'
);

fixtures_ok [ 
    'Filestore' => [
        {
            id                  => 1,
            uuid                => '40209d1c-fe2e-4354-83c8-fd3a6af9fd4a',
            thumbnail_uuid      => '7da4767b-7d11-4227-a556-d773a1bdc186',
            original_name       => 'Foo',
            size                => 23,
            mimetype            => 'application/test',
            md5                 => '127aec01313d29e0fe905c08a479444e',
            date_created        => 0,
            storage_location    => 'Drawer',
            is_archivable       => 1,
            virus_scan_status   => 'ok',
        },
        {
            id                  => 2,
            uuid                => 'd9653b65-2bb2-49ca-abd9-eca6afd34dd0',
            thumbnail_uuid      => '2949c1fd-00f9-4720-97dc-02cfb742e522',
            original_name       => 'Bar',
            size                => 67,
            mimetype            => 'application/test',
            md5                 => 'b05aab1f7088e69e6ac011fd2af8ffc1',
            date_created        => 0,
            storage_location    => 'Drawer',
            is_archivable       => 1,
            virus_scan_status   => 'found: test virus',
        },
    ]
], "Installed Fixtures";

my @rs_small = ResultSet('Filestore')->search( { size => { '<' => 100 } } );

ok scalar @rs_small == 1,
    "... found only 1 row";

ok @rs_small[0]->uuid eq '40209d1c-fe2e-4354-83c8-fd3a6af9fd4a',
    "... with the right uuid";

ok defined ResultSet('Filestore')->find(1),
    "... 'find' passes not 'found'";

ok ! defined ResultSet('Filestore')->find(2),
    "... 'find' skips that 'found: ...' virus";

done_testing();

1;