#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::Backend::Sysin::Modules::BAGCSV;

$zs->zs_transaction_ok(
    sub {
        my $m = Zaaksysteem::Backend::Sysin::Modules::BAGCSV->new;

        my $module_count;
        ok(($module_count = scalar($m->list_of_modules)),
            'Found a list of modules');

        $schema->resultset('Interface')->interface_create(
            {
                module => 'bagcsv',
                name   => 'BAG Import'
            },
        );
    },
    'Tested modules tested'
);


$zs->zs_transaction_ok(
    sub {
        my $m = Zaaksysteem::Backend::Sysin::Modules::BAGCSV->new;

        my $interface = $schema->resultset('Interface')->interface_create(
            {
                module       => 'key2finance',
                name         => 'BAG Import',
                case_type_id => $zs->create_zaaktype_ok->id
            },
        );

        my $attributelist = $interface->get_attribute_mapping;

        ### Change something in attribute mapping
        $attributelist->{attributes}->[2]->{checked} = 1;

        ### update into interface
        my $attributes = $interface->set_attribute_mapping($attributelist);

        ok(
            $attributes->{attributes}->[2]->{checked},
            'Valid checked optional attribute mapping field'
        );

    },
    'Tested: Attribute Processing'
);


zs_done_testing();
