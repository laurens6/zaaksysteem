package TestFor::General::Backed::Sysin::Interface::ResultSet;

use base 'ZSTest';

use TestSetup;

sub zs_interfaces : Tests {
    my $interface = $zs->create_interface_ok(active => 0);

    my $rs = $zs->schema->resultset('Interface');

    my $found = $rs->search_active({ name => $interface->name});
    is($found->count, 0, "No active interfaces found");

    {
        $interface->update({ active => 1 });
        my $found = $rs->search_active({ name => $interface->name });
        is($found->count, 1, "One active interface found");
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
