package TestFor::General::Object::Roles::MetaType;

use base 'ZSTest';

use TestSetup;

use Test::DummyObject;
use Test::MetaTypeImplementer;

use Zaaksysteem::Object::Roles::MetaType;

sub zs_object_roles_metatype_basic : Tests {
    my $meta = _make_meta();

    my $object;
    
    lives_ok { $object = $meta->new_object } 'basic object instantiation';

    can_ok $object, qw[
        type_reference
        get_string_fetchers
    ];

    ok $object->type_reference->does('Zaaksysteem::Object::Reference'),
        'object instance returns type reference';
}

sub zs_object_roles_metatype_roles : Tests {
    my $meta = _make_meta(
        instance_roles => [ 'Zaaksysteem::Object::Roles::Type' ]
    );

    my $object = $meta->new_object;

    ok $object->does('Zaaksysteem::Object::Roles::Type'), 'instance_roles applied';
}

sub zs_object_roles_metatype_attrs : Tests {
    my $attr = {
        name => 'attribute.derp',
        attribute_type => 'text',
        label => 'Derpstring',
    };

    my $meta = _make_meta(
        instance_attributes => [ $attr ]
    );

    my $object = $meta->new_object(derp => 'foo');

    can_ok $object, 'derp';

    is $object->derp, 'foo', 'constructor argument value set';
}

sub zs_object_roles_metatype_rels : Tests {
    my $any_object_rel = {
        name => 'related_object',
        label => 'Related object'
    };

    my $test_object_rel = {
        name => 'test_object',
        label => 'Test object',
        type => 'dummy_object'
    };

    my $meta = _make_meta(
        instance_relations => [ $any_object_rel, $test_object_rel ]
    );

    my $object = $meta->new_object;

    isa_ok $object, 'Zaaksysteem::Object', 'meta instantiates object';

    can_ok $object, qw[related_object test_object];

    lives_ok { $object->related_object($object) }
        'object accepts object as value for relation attribute';

    dies_ok { $object->test_object($object) }
        'object rejects object as value for test relation attribute';

    lives_ok { $object->test_object(Test::DummyObject->new) }
        'object accepts test object as value for test relation attribute';
}

sub _make_meta {
    my $type = Test::MetaTypeImplementer->new(
        id => 'de305d54-75b4-431b-adb2-eb6b9e546014',
        @_
    );

    my $meta;

    $type->instance_meta_class;

    lives_ok { $meta = $type->instance_meta_class } 'metatype builds meta';

    return $meta;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
