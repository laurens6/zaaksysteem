#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use Zaaksysteem::Types::MappedString;

use JSON;

use Zaaksysteem::Object::Attribute;

###
###
### Single Zaaksysteem::Object::Attribute
###
###
$zs->zs_transaction_ok(sub {
    throws_ok(
        sub {
            Zaaksysteem::Object::Attribute->new();
        },
        qr/Attribute \(.*?\) is required/,
        'Check required attributes'
    );

    throws_ok(
        sub {
            Zaaksysteem::Object::Attribute->new(
                name                => 'world',
                attribute_type      => 'textnot',
                value               => '1337',
            );
        },
        qr/Attribute \(attribute_type\) does not pass the type constraint/,
        'Check attribute validation'
    );

    throws_ok(
        sub {
            Zaaksysteem::Object::Attribute->new(
                name                => [],
                attribute_type      => 'text',
                value               => '1337',
            );
        },
        qr/Attribute \(name\).*?Validation failed for 'Str'/,
        'Check attribute "name" validation'
    );


    my $attribute   = Zaaksysteem::Object::Attribute->new(
        name                => 'world',
        attribute_type      => 'text',
        value               => '1337',
        human_value         => 'hello',
    );

    isa_ok($attribute, 'Zaaksysteem::Object::Attribute');

    can_ok(
        $attribute,
        qw/
            attribute_type
            value
            human_value
            object_table
            object_id
            is_systeemkenmerk
            is_filter
        /
    );

    is($attribute->human_value, '1337', "Human value of simple text attribute");
    is($attribute->index_value, '1337', "Index value of simple text attribute");

    $attribute = Zaaksysteem::Object::Attribute->new(
        name                => 'world',
        attribute_type      => 'text',
        value               => Zaaksysteem::Types::MappedString->new(
            original => 'foreignese',
            mapped   => 'frontendese',
        ),
        human_value         => 'hello',
    );
    is($attribute->human_value, 'frontendese', "Human value of MappedString text attribute");
    is($attribute->index_value, 'foreignese', "Index value of MappedString text attribute");

}, 'Verified Zaaksysteem::Object::Attribute syntax');

$zs->zs_transaction_ok(sub {
    my $map         = {
        'name'              => 'world',
        'attribute_type'    => 'text',
        'value'             => 'hello',
    };

    my $attribute   = Zaaksysteem::Object::Attribute->new($map);

    is($attribute->value, $attribute->human_value, 'Human value identical to value');
    my $json = $attribute->TO_JSON;

    isa_ok($json, 'HASH');

    is($json->{ $_ }, $attribute->$_, 'Correct JSON response for attribute "' . $_ . '"')
        for (keys %{ $map }, 'human_value');
}, 'Checked role "Attribute::Text"');

$zs->zs_transaction_ok(sub {
    my $attribute = Zaaksysteem::Object::Attribute->new(
        'name'           => 'world',
        'attribute_type' => 'text',
        'value'          => ['hello', 'multivalue', 'world'],
    );

    is($attribute->value, $attribute->human_value, 'Human value identical to value');
    my $json = $attribute->TO_JSON;

    is_deeply(
        $attribute->index_value,
        $attribute->value,
        "Index value is identival to value"
    );
}, 'Checked multivalue "Attribute::Text" instances');

zs_done_testing();

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

