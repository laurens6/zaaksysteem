#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::DocumentValidator;

$zs->zs_transaction_ok(
    sub {

        my $case     = $zs->create_case_ok;
        my $sjabloon = $zs->create_sjabloon_ok;

        my $dir  = 't/inc/Documents';
        my $file = 'openoffice_document.odt';

        my $ok = Zaaksysteem::DocumentValidator->validate_path({filepath => "$dir/$file"});
        ok($ok, "Filename and MIME-type are OK");

        my $filestore = $zs->create_filestore_ok(
            file_path     => "$dir/$file",
            original_name => $file,
        );
        $sjabloon->filestore($filestore);

        my $document = $sjabloon->_get_odf_document_handle({tmpdir => '/tmp'});
        isa_ok($document, "OpenOffice::OODoc::Document");

        $sjabloon->_filestore_operations({
            file   => "/tmp/$file",
            dir    => $dir,
            format => 'odt',
            case   => $case,
        });
        $ok = Zaaksysteem::DocumentValidator->validate_path({filepath => "/tmp/$file"});
        ok($ok, "Filename and MIME-type are OK");
    }, "An example document saved by Zaaksysteem should pass its own test",
);
zs_done_testing();
