package ClamAV::Client;
use Moose;

sub ping { 1 }

sub scan_stream {
    my ($self, $fh) = @_;
    my $content = join "", <$fh>;
    return $content eq 'i am infected';
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

