import {
    openPageAs
} from './../../../../functions/common/navigate';

describe('when logging in as behandelaar', () => {
    beforeAll(() => {
        openPageAs('behandelaar');
    });

    describe('and opening the documentintake', () => {
        beforeAll(() => {
            browser.get('/zaak/intake?scope=documents');
        });

        it('should not have the type selector present', () => {
            expect($('.document-list-visibility-select').isDisplayed()).toBe(false); 
        });
    });
});

describe('when logging in as documentintaker', () => {
    beforeAll(() => {
        openPageAs('documentintaker');
    });

    describe('and opening the documentintake', () => {
        beforeAll(() => {
            browser.get('/zaak/intake?scope=documents');
        });

        it('should have the type selector present', () => {
            expect($('.document-list-visibility-select').isDisplayed()).toBe(true); 
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
