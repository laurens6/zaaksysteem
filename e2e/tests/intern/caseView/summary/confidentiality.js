import {
    openPageAs
} from './../../../../functions/common/navigate';
import {
    updateConfidentiality,
    getAboutValue
} from './../../../../functions/intern/caseView/caseMenu';

describe('when opening case 72', () => {

    let attrValue = $('[data-name="vertrouwelijkheidsniveau"] .value-item');

    beforeAll(() => {
    
        openPageAs('admin', 72);
    
    });

    describe('and when updating the confidentiality to confidential', () => {
    
        beforeAll(() => {
        
            updateConfidentiality('vertrouwelijk');
        
        });

        it('the confidentiality should have changed to confidential', () => {

            expect(getAboutValue('Openbaarheid')).toEqual('Vertrouwelijk');
    
        });

        it('the rules should have updated using the confidential confidentiality', () => {
        
            expect(attrValue.getText()).toEqual('Vertrouwelijk');
    
        });
    
        describe('and when updating the confidentiality to internal', () => {
        
            beforeAll(() => {
        
                updateConfidentiality('intern');

            });
        
            it('the confidentiality should have changed to internal', () => {

                expect(getAboutValue('Openbaarheid')).toEqual('Intern');
    
            });

            it('the rules should have updated using the internal confidentiality', () => {
            
                expect(attrValue.getText()).toEqual('Intern');
        
            });

            describe('and when updating the confidentiality to public', () => {
            
                beforeAll(() => {
        
                    updateConfidentiality('openbaar');

                });
            
                it('the confidentiality should have changed to public', () => {

                    expect(getAboutValue('Openbaarheid')).toEqual('Openbaar');

                });

                it('the rules should have updated using the public confidentiality', () => {
                
                    expect(attrValue.getText()).toEqual('Openbaar');
            
                });
            
            });

        });
    
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
