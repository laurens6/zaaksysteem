import {
    openPage,
    openPageAs
} from './../../../functions/common/navigate';
import {
    checkObjectButtonPresence,
    advance,
    createObjectMutationCreate,
    createObjectMutationMutate,
    createObjectMutationDelete,
    getAdvanceButtonText,
    editObjectMutation,
    startObjectMutation,
    performObjectMutationAction,
    deleteObjectMutation
} from './../../../functions/intern/caseView/casePhase';
import {
    openPhase
} from './../../../functions/intern/caseView/caseNav';
import waitForElement from './../../../functions/common/waitForElement';
import {
    getValue
} from './../../../functions/common/input/caseAttribute';
import {
    selectFirstSuggestion
} from './../../../functions/common/select';

const caseMutations = $$('.case-mutation');
const form = $('zs-case-object-mutation-form');

describe('when opening case 111', () => {

    beforeAll(() => {
        openPageAs('admin', 111);
    });

    it('there should only be the option to create an object', () => {
        expect(checkObjectButtonPresence('objectmutatie')).toEqual(['aanmaken']);
    });

    describe('and when creating an object mutation of type "create"', () => {
        beforeAll(() => {
            const testData = [
                {
                    attr: form.$('[data-name="objectmutatie_titel"]'),
                    input: 'objectmutatie aanmaken 111'
                },
                {
                    attr: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: 'objectmutatie aanmaken 111'
                },
                {
                    attr: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                    input: 3
                },
                {
                    attr: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 1, 3 ]
                },
                {
                    attr: form.$('[data-name="objectmutatie_datum"]'),
                    input: '13-01-2010'
                },
                {
                    attr: form.$('[data-name="objectmutatie_valuta"]'),
                    input: '12.34'
                },
                {
                    attr: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['straat 4']
                }
            ];

            createObjectMutationCreate('objectmutatie', 'aanmaken', testData);
        });

        it('the object mutation should be present', () => {
            expect(caseMutations.count()).toEqual(1);
        });

        it('the object mutation tooltip should indicate that it has not been processed yet', () => {
            expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Aan te maken');
        });

        it('the object mutation should have a button for editing', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(true);
        });

        it('the object mutation should not have a button for opening', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(false);
        });

        it('the object mutation should have a button for deleting', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(true);
        });
    });
});

describe('when opening case 112 and creating an object', () => {
    beforeAll(() => {
        openPageAs('admin', 112);
        advance();
        waitForElement('zs-case-result');
        openPage(112);
        openPhase(2);
    });

    it('the object mutation should be present', () => {
        expect(caseMutations.count()).toEqual(1);
    });

    it('the object mutation tooltip should indicate that it has been processed', () => {
        expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Aangemaakt');
    });

    it('the object mutation should not have a button for editing', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);
    });

    it('the object mutation should have a button for opening', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);
    });

    it('the object mutation should not have a button for deleting', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(false);
    });
});

describe('when opening case 113', () => {
    beforeAll(() => {
        openPageAs('admin', 113);
    });

    it('there should only be the option to mutate an object', () => {
        expect(checkObjectButtonPresence('objectmutatie')).toEqual(['bewerken']);
    });

    describe('and when creating an object mutation of type "mutate"', () => {
        beforeAll(() => {
            const testData = [
                {
                    attr: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: 'objectmutatie gewijzigd 113'
                },
                {
                    attr: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                    input: 1
                },
                {
                    attr: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 2 ]
                },
                {
                    attr: form.$('[data-name="objectmutatie_datum"]'),
                    input: '20-04-2015'
                },
                {
                    attr: form.$('[data-name="objectmutatie_valuta"]'),
                    input: '43,21'
                },
                {
                    attr: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['weg 7']
                }
            ];

            createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate aanmaken', testData);
        });

        it('the object mutation should be present', () => {
            expect(caseMutations.count()).toEqual(1);
        });

        it('the object mutation tooltip should indicate that it has not been processed yet', () => {
            expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Te wijzigen');
        });

        it('the object mutation should have a button for editing', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(true);
        });

        it('the object mutation should have a button for opening', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);
        });

        it('the object mutation should have a button for deleting', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(true);
        });
    });
});

describe('when opening case 114 and mutating an object', () => {
    beforeAll(() => {
        openPageAs('admin', 114);
        advance();
        waitForElement('zs-case-result');
        openPage(114);
        openPhase(2);
    });

    it('the object mutation should be present', () => {
        expect(caseMutations.count()).toEqual(1);
    });

    it('the object mutation tooltip should indicate that it has been processed', () => {
        expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Gewijzigd');
    });

    it('the object mutation should not have a button for editing', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);
    });

    it('the object mutation should have a button for opening', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);
    });

    it('the object mutation should not have a button for deleting', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(false);
    });
});

describe('when opening case 115', () => {
    beforeAll(() => {
        openPageAs('admin', 115);
    });

    it('there should only be the option to delete an object', () => {
        expect(checkObjectButtonPresence('objectmutatie')).toEqual(['verwijderen']);
    });

    describe('and when creating an object mutation of type "delete"', () => {
        beforeAll(() => {
            createObjectMutationDelete('objectmutatie', 'verwijderen', 'Delete aanmaken');
        });

        it('the object mutation should be present', () => {
            expect(caseMutations.count()).toEqual(1);
        });

        it('the object mutation tooltip should indicate that it has not been processed yet', () => {
            expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Te verwijderen');
        });

        it('the object mutation should not have a button for editing', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);
        });

        it('the object mutation should have a button for opening', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(true);
        });

        it('the object mutation should have a button for deleting', () => {
            expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(true);
        });
    });
});

describe('when opening case 116 and deleting an object', () => {
    beforeAll(() => {
        openPageAs('admin', 116);
        advance();
        waitForElement('zs-case-result');
        openPage(116);
        openPhase(2);
    });

    it('the object mutation should be present', () => {
        expect(caseMutations.count()).toEqual(1);
    });

    it('the object mutation tooltip should indicate that it has been processed', () => {
        expect(caseMutations.get(0).$('.case-mutation-icon i').getAttribute('zs-tooltip')).toEqual('Verwijderd');
    });

    it('the object mutation should not have a button for editing', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .edit').isDisplayed()).toBe(false);
    });

    it('the object mutation should not have a button for opening', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .mdi-launch').isDisplayed()).toBe(false);
    });

    it('the object mutation should not have a button for deleting', () => {
        expect(caseMutations.get(0).$('.case-mutation-actions .delete').isDisplayed()).toBe(false);
    });
});

describe('when opening case 117 and creating an object mutation of type "create" without all the required information', () => {
    beforeAll(() => {
        openPageAs('admin', 117);

        const testData = [
            {
                attr: form.$('[data-name="objectmutatie_titel"]'),
                input: 'objectmutatie aanmaken 117'
            },
            {
                attr: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                input: 3
            },
            {
                attr: form.$('[data-name="objectmutatie_datum"]'),
                input: '13-01-2010'
            },
            {
                attr: form.$('[data-name="objectmutatie_valuta"]'),
                input: '12.34'
            }
        ];

        createObjectMutationCreate('objectmutatie', 'aanmaken', testData);
    });

    it('the object mutation should be present', () => {
        expect(caseMutations.count()).toEqual(1);
    });

    it('the object mutation should indicate that it is not complete', () => {
        expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(true);
    });

    it('the case should be unable to advance', () => {
        expect(getAdvanceButtonText()).toEqual('fase incompleet');
    });

    describe('and when completing the object mutation', () => {
        beforeAll(() => {
            const testData = [
                {
                    attr: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: 'objectmutatie aanmaken 117'
                },
                {
                    attr: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 1, 3 ]
                },
                {
                    attr: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['straat 4']
                }
            ];

            editObjectMutation('objectmutatie', 1, testData);
        });

        it('the object mutation should be present', () => {
            expect(caseMutations.count()).toEqual(1);
        });

        it('the object mutation should indicate that it is not complete', () => {
            expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(false);
        });

        it('the case should be unable to advance', () => {
            expect(getAdvanceButtonText()).toEqual('fase afronden');
        });
    });
});

describe('when opening case 118 and creating an object mutation of type "mutate" without all the required information', () => {
    beforeAll(() => {
        const testData = [
            {
                attr: form.$('[data-name="objectmutatie_titel"]'),
                input: 'objectmutatie aanmaken 118'
            },
            {
                attr: form.$('[data-name="objectmutatie_enkelvoudige_keuze"]'),
                input: 3
            },
            {
                attr: form.$('[data-name="objectmutatie_datum"]'),
                input: '13-01-2010'
            },
            {
                attr: form.$('[data-name="objectmutatie_valuta"]'),
                input: '12.34'
            }
        ];

        openPageAs('admin', 118);
        createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate without info', testData);
    });

    it('the object mutation should be present', () => {
        expect(caseMutations.count()).toEqual(1);
    });

    xit('the object mutation should indicate that it is not complete', () => {
        expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(true);
    });

    xit('the case should be unable to advance', () => {
        expect(getAdvanceButtonText()).toEqual('fase incompleet');
    });

    describe('and when completing the object mutation', () => {
        beforeAll(() => {
            const testData = [
                {
                    attr: form.$('[data-name="objectmutatie_tekstveld"]'),
                    input: 'objectmutatie aanmaken 118'
                },
                {
                    attr: form.$('[data-name="objectmutatie_meervoudige_keuze"]'),
                    input: [ 1, 3 ]
                },
                {
                    attr: form.$('[data-name="objectmutatie_adres_dmv_straatnaam"]'),
                    input: ['straat 4']
                }
            ];

            editObjectMutation('objectmutatie', 1, testData);
        });

        it('the object mutation should be present', () => {
            expect(caseMutations.count()).toEqual(1);
        });

        it('the object mutation should indicate that it is not complete', () => {
            expect($('.case-mutation .mdi-alert-circle').isDisplayed()).toBe(false);
        });

        it('the case should be unable to advance', () => {
            expect(getAdvanceButtonText()).toEqual('fase afronden');
        });
    });
});

describe('when opening case 119 and starting an object mutation of type "create"', () => {
    beforeAll(() => {
        openPageAs('admin', 119);
        startObjectMutation('objectmutatie', 'aanmaken');
    });

    it('the object mutation should have the values of the case prefilled', () => {
        const testData = [
            {
                attrName: 'titel',
                result: ''
            },
            {
                attrName: 'tekstveld',
                result: 'Prefilled'
            },
            {
                attrName: 'enkelvoudige_keuze',
                result: 'objectmutatie optie 1'
            },
            {
                attrName: 'meervoudige_keuze',
                result: [ false, true, true ]
            },
            {
                attrName: 'datum',
                result: '2016-04-20'
            },
            {
                attrName: 'valuta',
                result: '0,13'
            },
            {
                attrName: 'adres_dmv_straatnaam',
                result: 'Klimmen - Achtbunderstraat 2'
            }
        ];

        testData.forEach(thing => {
            const { attrName, result } = thing;

            expect(getValue(form.$(`[data-name="objectmutatie_${attrName}"`))).toEqual(result);
        });
    });
});

describe('when opening case 120 and starting an object mutation of type "mutate"', () => {
    beforeAll(() => {
        openPageAs('admin', 120);
        startObjectMutation('objectmutatie', 'bewerken');
        selectFirstSuggestion(form.$('vorm-field input'), 'Mutate prefill');
        waitForElement('zs-case-object-mutation-form [data-name="objectmutatie_titel"]');
    });

    it('the object mutation should have the values of the case prefilled', () => {
        const testData = [
            {
                attrName: 'titel',
                result: 'Mutate prefill'
            },
            {
                attrName: 'tekstveld',
                result: 'Prefilled'
            },
            {
                attrName: 'enkelvoudige_keuze',
                result: 'objectmutatie optie 1'
            },
            {
                attrName: 'meervoudige_keuze',
                result: [ false, true, true ]
            },
            {
                attrName: 'datum',
                result: '2016-04-20'
            },
            {
                attrName: 'valuta',
                result: '0,13'
            },
            {
                attrName: 'adres_dmv_straatnaam',
                result: 'Klimmen - Achtbunderstraat 2'
            }
        ];

        testData.forEach(thing => {
            const { attrName, result } = thing;

            expect(getValue(form.$(`[data-name="objectmutatie_${attrName}"`))).toEqual(result);
        });

    });

});

describe('when opening case 122 and creating an object mutation of type "mutate" for an object with an existing unprocessed object mutation', () => {
    beforeAll(() => {
        openPageAs('admin', 122);
        createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate mutated not processed');
    });

    it('the object mutation should not have been created', () => {
        expect(caseMutations.count()).toEqual(0);
    });
});

describe('when opening case 124 and creating an object mutation of type "mutate" for an object with an existing processed object mutation', () => {
    beforeAll(() => {
        openPageAs('admin', 124);
        createObjectMutationMutate('objectmutatie', 'bewerken', 'Mutate mutated processed');
    });

    it('the object mutation should have been created', () => {
        expect(caseMutations.count()).toEqual(1);
    });
});

describe('when opening case 126 and creating an object mutation of type "create" and cancelling it', () => {
    beforeAll(() => {
        openPageAs('admin', 126);
        startObjectMutation('objectmutatie', 'aanmaken');
        performObjectMutationAction('cancel');
    });

    it('the object mutation should not have been created', () => {
        expect(caseMutations.count()).toEqual(0);
    });
});

describe('when opening case 127 and deleting an object mutation', () => {
    beforeAll(() => {
        openPageAs('admin', 127);
        deleteObjectMutation('objectmutatie', 1);
    });

    it('the object mutation should not be present anymore', () => {
        expect(caseMutations.count()).toEqual(0);
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
