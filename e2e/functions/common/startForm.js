import casetypes from './../../utilities/casetypes';
import citizens from './../../utilities/citizens';
import organisations from './../../utilities/organisations';
import employees from './../../utilities/employees';
import findUtilityProperty from './findUtilityProperty';

const getUtility = type => {
    const utilities = {
        citizen: citizens,
        organisation: organisations,
        employee: employees
    };

    return utilities[type];
};

export default data => {
    const channelOfContact = data.channelOfContact;
    const casetypeUuid = findUtilityProperty(casetypes, 'name', data.casetype, 'uuid');
    const requestorUuid = findUtilityProperty(getUtility(data.requestorType), 'id', data.requestorId, 'uuid');
    const recipientUuid = data.recipientId ? findUtilityProperty(getUtility(data.recipientType), 'id', data.recipientId, 'uuid') : '';
    const recipientString = `&ontvanger=${recipientUuid}`;

    browser.get(`${browser.baseUrl}/intern/aanvragen/${casetypeUuid}/?aanvrager=${requestorUuid}${recipientString}&contactkanaal=${channelOfContact}`);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
