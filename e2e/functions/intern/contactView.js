export const openContactInfo = () => {
    $('[id="ui-id-12"]').click();
};

export const getEmailAddress = () => $('[name="npc-email"]').getAttribute('value');
export const getPhoneNumber = () => $('[name="npc-telefoonnummer"]').getAttribute('value');
export const getMobileNumber = () => $('[name="npc-mobiel"]').getAttribute('value');

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
